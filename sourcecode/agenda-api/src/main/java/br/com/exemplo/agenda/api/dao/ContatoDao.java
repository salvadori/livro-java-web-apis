package br.com.exemplo.agenda.api.dao;

import java.util.List;

import br.com.exemplo.agenda.api.dominio.Contato;

public interface ContatoDao {

    void cadastrar(Contato contato);

    void alterar(Contato contato);

    void remover(String idContato);

    Contato consultar(String idContato);

    List<Contato> listarTodos();

}
