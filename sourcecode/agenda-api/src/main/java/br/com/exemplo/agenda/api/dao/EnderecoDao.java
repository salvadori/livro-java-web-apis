package br.com.exemplo.agenda.api.dao;

import br.com.exemplo.agenda.api.dominio.Endereco;

public interface EnderecoDao {

    void cadastrar(Endereco endereco, String idContato);

    Endereco consultar(String idContato);

    void remover(String idContato);

}
