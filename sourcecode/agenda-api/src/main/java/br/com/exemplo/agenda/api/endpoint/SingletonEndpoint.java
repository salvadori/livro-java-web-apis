package br.com.exemplo.agenda.api.endpoint;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

@Component
@Path("singleton")
public class SingletonEndpoint {
    int acessos = 0;

    @GET
    public Response teste() {
        acessos++;
        System.out.println(acessos);
        return Response.ok(acessos).build();
    }
}
