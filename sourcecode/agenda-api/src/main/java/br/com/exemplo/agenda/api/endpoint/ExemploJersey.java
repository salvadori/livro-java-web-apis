package br.com.exemplo.agenda.api.endpoint;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("caminho1")
public class ExemploJersey {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("caminho2/{var1}")
    public Response carregarUmRecurso(@PathParam("var1") String var1, @QueryParam("var2") String var2) {
        // codigo para carregar um recurso
        String retorno = String.format("var1: %s var2: %s", var1, var2);
        return Response.ok(retorno).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response criarUmRecurso() {
        // codigo para criar um recurso
        return Response.ok("mensagem de retorno").build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response modificarUmRecurso() {
        // codigopara modificar um recurso
        return Response.ok("mensagem de retorno").build();
    }

    @DELETE
    public Response removerUmRecurso() {
        // codigo para remover um recurso
        return Response.ok("mensagem de retorno").build();
    }

}
