package br.com.exemplo.agenda.api.negocio;

@SuppressWarnings("serial")
public class ContatoNaoEncontradoException extends RuntimeException {

    public ContatoNaoEncontradoException(String msg) {
        super(msg);
    }

}
