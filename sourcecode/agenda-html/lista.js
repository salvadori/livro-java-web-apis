var listarTodosContatos = function() {
  pathLista = $('#apiPath').val()+"/listaDeContatos";
  pathContato = $('#apiPath').val()+"/contato?idContato=";

  $.ajax({
    url: pathLista,
    type: 'GET',
    async: true,
    contentType: 'application/json',
    success: function(contatos) {
      limparTabela();
      $.each(contatos, function(index, contato) {
        var novaLinha =
          '<tr>' +
          '<td>' + contato.nome + '</td>' +
          '<td>' + contato.email + '</td>' +
          '<td>' + contato.cpf + '</td>' +
          '<td><a href="#" onclick=consultar("'+pathContato+contato.id+'")>consultar</a></td>' +
          '<td><a href="#" onclick=alterar("'+pathContato+contato.id+ '")>alterar</a></td>' +
          '<td><a href="#" onclick=remover("'+pathContato+contato.id+ '")>remover</a></td>' +
          '</tr>';
        $("#tabelaContatos tr:last").after(novaLinha);
      });
    },
    error: function() {

    }
  });
};

var limparTabela = function() {
  $("#tabelaContatos").find("tr:gt(0)").remove();
}

var consultar = function(urlContato) {
  sessionStorage.setItem("urlContato", urlContato);
  window.location.href = "consulta.html";
}

var alterar = function(urlContato) {
  sessionStorage.setItem("urlContato", urlContato);
  window.location.href = "alteracao.html";
}

var remover = function(urlContato) {
  $.ajax({
    url: urlContato,
    type: 'DELETE',
    async: true,
    success: function() {
      listarTodosContatos();
    }
  });
}

$(document).ready(function() {
  $("#botaoListarTodos").click(function() {
    listarTodosContatos();
  });

  $("#botaoCadastrar").click(function() {
    window.location.href = "cadastro.html";
  });
});
