var cadastrar = function() {
  var dadosContato = {
    nome: $("#nome").val(),
    email: $("#email").val(),
    cpf: $("#cpf").val(),
    telefone: $("#telefone").val(),
    dataNascimento: $("#datan").val(),
    estado: $("#estado").val(),
    cidade: $("#cidade").val(),
    bairro: $("#bairro").val(),
    logradouro: $("#logradouro").val()
  };
  $.ajax({
    url: "http://localhost:8080/agenda-api/listaDeContatos",
    type: 'POST',
    async: true,
    contentType: 'application/json',
    data: JSON.stringify(dadosContato),
    success: function() {
      $("#resultado").empty();
      $("#resultado").append("Contato cadastrado com sucesso")

    },
    error: function(xhr, status, error) {
      $("#resultado").empty();
      $("#resultado").append("Erro ao cadastrar: " + xhr.responseText)
    }
  });
};
$(document).ready(function() {
  $("#botaoCadastrar").click(function() {
    cadastrar();
  });
});
